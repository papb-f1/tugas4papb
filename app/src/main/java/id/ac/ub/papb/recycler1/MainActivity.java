package id.ac.ub.papb.recycler1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv1;
    public static String TAG = "RV1";

//    private Button btSimpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rv1 = findViewById(R.id.rv1);
        ArrayList<Mahasiswa> data = getData();
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
        rv1.setAdapter(adapter);
        rv1.setLayoutManager(new LinearLayoutManager(this));


        Button btSimpan = findViewById(R.id.btSimpan);
        btSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText nim = findViewById(R.id.etNim);
                EditText nama = findViewById(R.id.editTextTextPersonName2);

                Mahasiswa mahasiswa = new Mahasiswa();
                mahasiswa.nim = nim.getText().toString();
                mahasiswa.nama = nama.getText().toString();
                data.add(mahasiswa);

                MahasiswaAdapter adapter1 = new MahasiswaAdapter(MainActivity.this, data);
                rv1.setAdapter(adapter1);
                rv1.setLayoutManager(new LinearLayoutManager(MainActivity.this));

                Toast.makeText(getApplicationContext(), "Data telah disimpan!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.nim));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }

//    btSimpan.OnClickListener(new View.OnClickListener() {
//        @Override
//        public void onClick(View v) {
//            // Tambahkan kode yang ingin Anda jalankan saat tombol "Simpan" diklik di sini.
//            // Misalnya, Anda bisa menyimpan data ke database atau menampilkan pesan Toast.
//            // Contoh:
//
//            Toast.makeText(getApplicationContext(), "Data telah disimpan!", Toast.LENGTH_SHORT).show();
//        }
//    });

//                setOnClickListener(new View.OnClickListener {
//        override fun onClick(v: View?) {
//            // Kode yang akan dijalankan saat tombol "Simpan" diklik
//            // Anda bisa menambahkan logika untuk menyimpan data di sini
//        }
}