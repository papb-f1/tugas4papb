package id.ac.ub.papb.recycler1;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class rowView extends RecyclerView.ViewHolder {
    ImageView imageView;
    TextView nimView, nameView;

    public rowView(@NonNull View itemView){
        super(itemView);
        imageView = itemView.findViewById(R.id.imageView);
        nimView = itemView.findViewById(R.id.tvNim);
        nameView = itemView.findViewById(R.id.tvNama);
    }
}
